import socket
import time
from typing import Any

from .serialize import GodotSerialize


class GodotUDPCom:
    MAX_PACKET_SIZE = 65_500

    def __init__(self, address: str, port: int, timeout: float = None):
        self.port = port
        self.address = address
        self.timeout = timeout

        # Creating TCP socket server
        self._send_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self._recv_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        if timeout >= 0.0:
            self._recv_socket.settimeout(timeout)
        self._recv_socket.setblocking(timeout != 0.0)
        self._recv_socket.bind((self.address, port + 1))
        self._message_len: int = 0
        self._message: bytes = b''

        self._acknowledge_packet = GodotSerialize.dumps(True)

    def send_message(self, action: str, data: Any):
        self._message = GodotSerialize.dumps({'action': action, 'data': data})
        self._message = GodotSerialize.dumps(len(self._message)) + self._message
        # print(f'Sending message (len {len(self._message)}) : '
        #       + ' '.join([self._message.hex()[i:i+4] for i in range(0, len(self._message.hex()), 4)]))
        if len(self._message) > self.MAX_PACKET_SIZE:
            split_index = 0
            for split_index in range(0, len(self._message), self.MAX_PACKET_SIZE):
                self._send_socket.sendto(
                    self._message[split_index:split_index + self.MAX_PACKET_SIZE], (self.address, self.port))
                # Receive ACK
                self._recv_socket.recv(self.MAX_PACKET_SIZE)
        else:
            self._send_socket.sendto(self._message, (self.address, self.port))
            # Receive ACK
            self._recv_socket.recv(self.MAX_PACKET_SIZE)

    def receive_message(self) -> Any:
        self._message = self._recv_socket.recv(self.MAX_PACKET_SIZE)
        self._message_len = GodotSerialize.loads(self._message[:8])
        self._message = self._message[8:]
        # Send ACK
        self._send_socket.sendto(self._acknowledge_packet, (self.address, self.port))
        if len(self._message) < self._message_len:
            max_len = len(self._message)
            buf = self._message
            while len(buf) + max_len <= self._message_len:
                self._message = self._recv_socket.recv(self._message_len)
                self._send_socket.sendto(self._acknowledge_packet, (self.address, self.port))
                buf += self._message
            if len(buf) < self._message_len:
                buf += self._recv_socket.recv(self._message_len - len(buf))
                self._send_socket.sendto(self._acknowledge_packet, (self.address, self.port))
            self._message = buf
        # print('Received : ' + ' '.join([self._message.hex()[i:i+4] for i in range(0, len(self._message.hex()), 4)]))
        return GodotSerialize.loads(self._message)

    def finalize(self):
        self._recv_socket.setblocking(True)
        self._recv_socket.settimeout(1.0)
        try:
            while True:
                self._recv_socket.recv(self.MAX_PACKET_SIZE)
                self._send_socket.sendto(self._acknowledge_packet, (self.address, self.port))
        except socket.timeout:
            pass
        self._recv_socket.settimeout(self.timeout)
        if self.timeout == 0.0:
            self._recv_socket.setblocking(False)
