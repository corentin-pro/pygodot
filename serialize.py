import struct
from typing import Any, Tuple


class GodotSerialize:
    FLOAT_MAX = float(2**127) * (2 - 2**-23)
    FLOAT_MIN = 2**-149
    FLOAT_INF = float('inf')

    @staticmethod
    def _check_double(value: float):
        return value != value or (  # only nan is different to itself
            (value > GodotSerialize.FLOAT_MAX and value != GodotSerialize.FLOAT_INF)
            or (value < -GodotSerialize.FLOAT_MAX and value != -GodotSerialize.FLOAT_INF)
            or (value > 0 and value < GodotSerialize.FLOAT_MIN)
            or (value < 0 and value > -GodotSerialize.FLOAT_MIN))

    @staticmethod
    def dumps(data) -> bytes:
        if data is None:
            return b'\x00\x00\x00\x00'
        elif isinstance(data, bool):
            return struct.pack('<II', 1, 1 if data else 0)
        elif isinstance(data, int):
            return struct.pack('<Ii', 2, data)
        elif isinstance(data, float):
            if GodotSerialize._check_double(data):
                return struct.pack('<Id', 65539, data)
            return struct.pack('<If', 3, data)
        elif isinstance(data, str):
            encoded = data.encode()
            return struct.pack('<II', 4, len(encoded)) + encoded + (b'\x00' * ((4 - (len(encoded) % 4)) % 4))
        elif isinstance(data, tuple):
            if len(data) == 2:
                return struct.pack('<Iff', 5, *[float(value) for value in data])
            elif len(data) == 4:
                return struct.pack('<Iffff', 6, *[float(value) for value in data])
            elif len(data) == 3:
                return struct.pack('<Ifff', 7, *[float(value) for value in data])
            elif len(data) == 6:
                return struct.pack('<Iffffff', 8, *[float(value) for value in data])
        elif isinstance(data, dict):
            result = struct.pack('<II', 18, len(data))
            for key in data:
                result += GodotSerialize.dumps(key)
                result += GodotSerialize.dumps(data[key])
            return result
        elif isinstance(data, bytes):
            return struct.pack('<II', 20, len(data)) + data + (b'\x00' * ((4 - (len(data) % 4)) % 4))
        elif isinstance(data, list):
            if len(data) == 0:
                return b'\x19\x00\x00\x00\x00\x00\x00\x00'
            # one liner checks for type with python all() is way too slow so manual checking is done
            types = [int, float, str, tuple]
            first_type = type(data[0])
            tuple_length = len(data[0]) if first_type == tuple else 0
            if first_type in types:
                consistent_type = True
                if first_type == tuple:
                    for value in data:
                        if type(value) != first_type or len(value) != tuple_length:
                            consistent_type = False
                            break
                else:
                    for value in data:
                        if type(value) != first_type:
                            consistent_type = False
                            break
                if consistent_type:
                    if first_type == int:
                        return struct.pack('<II' + ('i' * len(data)), 21, len(data), *data)
                    elif first_type == float:
                        return struct.pack('<II' + ('f' * len(data)), 22, len(data), *data)
                    elif first_type == str:
                        result = struct.pack('<II', 23, len(data))
                        for value in data:
                            encoded = value.encode()
                            result += struct.pack('<I', len(encoded) + 1) + encoded + b'\x00' + (
                                b'\x00' * ((4 - ((len(encoded) + 1) % 4)) % 4))
                        return result
                    elif first_type == tuple:
                        if tuple_length == 2:
                            result = struct.pack('<II', 24, len(data))
                            for value in data:
                                result += struct.pack('<ff', *value)
                            return result
                        elif tuple_length == 3:
                            result = struct.pack('<II', 25, len(data))
                            for value in data:
                                result += struct.pack('<fff', *value)
                            return result
                        elif tuple_length == 4:
                            result = struct.pack('<II', 26, len(data))
                            for value in data:
                                result += struct.pack('<ffff', *value)
                            return result
            result = struct.pack('<II', 19, len(data))
            for value in data:
                result += GodotSerialize.dumps(value)
            return result

    @staticmethod
    def loads(data: bytes) -> Any:
        try:
            # print('loads : ' + ' '.join([data.hex()[i:i+4] for i in range(0, len(data.hex()), 4)]))
            return GodotSerialize._loads(data)[0]
        except TypeError:  # as error:
            return None

    @staticmethod
    def _loads(data: bytes) -> Tuple[Any, bytes]:
        if not data:
            return None
        data_type = struct.unpack('<I', data[:4])[0]
        data = data[4:]
        if data_type == 0:  # null
            return (None, data)
        elif data_type == 1:  # bool
            return (data[0] != 0, data[4:])
        elif data_type == 2:  # int
            return (struct.unpack('<i', data[:4])[0], data[4:])
        elif data_type == 3:  # float
            return (struct.unpack('<f', data[:4])[0], data[4:])
        elif data_type == 65539:  # double
            return (struct.unpack('<d', data[:8])[0], data[8:])
        elif data_type == 5:  # Vector2
            return (struct.unpack('<ff', data[:8]), data[8:])
        elif data_type == 6:  # rect2
            return (struct.unpack('<ffff', data[:16]), data[16:])
        elif data_type == 7:  # Vector3
            return (struct.unpack('<fff', data[:12]), data[12:])
        elif data_type == 8:  # transform2d
            return (struct.unpack('<ffffff', data[:24]), data[24:])

        data_len = struct.unpack('<i', data[:4])[0]
        data_align = (4 - (data_len % 4)) % 4
        data = data[4:]
        if data_type == 4:
            return (data[:data_len].decode(), data[data_len + data_align:])
        elif data_type == 18:
            result = {}
            for _key_index in range(data_len):
                key, data = GodotSerialize._loads(data)
                value, data = GodotSerialize._loads(data)
                result[key] = value
            return (result, data)
        elif data_type == 19:
            result = []
            for _ in range(data_len):
                value, data = GodotSerialize._loads(data)
                result.append(value)
            return result, data
        elif data_type == 20:
            return data[:data_len], data[data_len + data_align:]
        elif data_type == 21:
            return (list(struct.unpack('<' + ('i' * data_len), data[:data_len * 4])), data[data_len * 4:])
        elif data_type == 22:
            return (list(struct.unpack('<' + ('f' * data_len), data[:data_len * 4])), data[data_len * 4:])
        elif data_type == 23:
            result = []
            for _ in range(data_len):
                str_len = struct.unpack('<I', data[:4])[0]
                data_align = (4 - (str_len % 4)) % 4
                data = data[4:]
                result.append(data[:str_len - 1].decode())
                data = data[str_len + data_align:]
            return result, data
        elif data_type == 24:
            result = []
            for _ in range(data_len):
                result.append(struct.unpack('<ff', data[:8]))
                data = data[8:]
            return result, data
        elif data_type == 25:
            result = []
            for _ in range(data_len):
                result.append(struct.unpack('<fff', data[:12]))
                data = data[12:]
            return result, data
        elif data_type == 26:
            result = []
            for _ in range(data_len):
                result.append(struct.unpack('<ffff', data[:16]))
                data = data[16:]
            return result, data
        raise Exception(f'Type not supported : {data_type}')
